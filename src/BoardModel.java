
import java.util.Observable;

public class BoardModel extends Observable {

    private Player[] players = new Player[6];
    final String[] path = new String[]{"src/Image/Circle_Red.png",
        "src/Image/Circle_Blue.png", "src/Image/Circle_Green.png",
        "src/Image/Circle_Orange.png", "src/Image/Circle_Yellow.png",
        "src/Image/DrawingPin1_Blue.png",};
    private int noPlayers;
    private int turn = 0;
    private TheView view = new TheView(this);
    private Map[] maps = new Map[5];
    private int mapNo = 0;

    public BoardModel() {
        noPlayers = 0;
    }

    public BoardModel(int num) {

        players = new Player[num];  //create players array
        noPlayers = num;            // set number of player

        for (int i = 0; i < num; i++) { //create player and set name
            players[i] = new Player("Player " + (i + 1));
        }

        //create board array
        int[][] ladder = {
            {18, 37},
            {48, 67},
            {62, 79},
            {70, 89},};
        int[][] snakes = {
            {31, 13},
            {57, 35},
            {85, 63},};
        int cells = 100;
        int[][] size = {
            {800, 800},};
        int[][] XY = {
            {39, 739},};
        String path = "src/Image/board1.jpg";
        maps[2] = new Map(ladder, snakes, cells, size, path, XY);

        ladder = new int[][]{
            {8, 26},
            {21, 82},
            {43, 77},
            {50, 91},
            {54, 93},
            {62, 96},
            {66, 87},
            {80, 100},};
        snakes = new int[][]{
            {44, 19},
            {46, 5},
            {48, 9},
            {52, 11},
            {55, 7},
            {59, 17},
            {64, 36},
            {69, 33},
            {73, 1},
            {83, 19},
            {92, 51},
            {95, 24},
            {98, 28},};
        path = "src/Image/board2.jpg";
        maps[3] = new Map(ladder, snakes, cells, size, path, XY);

        ladder = new int[][]{
            {3, 20},
            {6, 14},
            {11, 28},
            {15, 34},
            {17, 74},
            {22, 37},
            {38, 59},
            {49, 67},
            {57, 76},
            {61, 78},
            {81, 98},
            {88, 91},};
        snakes = new int[][]{
            {8, 4},
            {18, 1},
            {26, 10},
            {39, 5},
            {51, 6},
            {54, 36},
            {56, 1},
            {60, 23},
            {75, 28},
            {83, 45},
            {85, 59},
            {90, 48},
            {92, 25},
            {97, 87},
            {99, 63},};
        path = "src/Image/board3.gif";
        maps[4] = new Map(ladder, snakes, cells, size, path, XY);

        ladder = new int[][]{
            {3, 11},
            {7, 25},
            {17, 34},
            {22, 52},
            {40, 54},
            {41, 57},
            {48, 64},};
        snakes = new int[][]{
            {14, 6},
            {39, 23},
            {45, 33},
            {47, 19},
            {53, 13},
            {61, 46},};
        cells = 64;
        size = new int[][]{
            {640, 640},};
        XY = new int[][]{
            {39, 599},};
        path = "src/Image/board5.jpg";
        maps[1] = new Map(ladder, snakes, cells, size, path, XY);

        ladder = new int[][]{
            {3, 22},
            {5, 8},
            {11, 26},
            {20, 29},};
        snakes = new int[][]{
            {17, 4},
            {19, 7},
            {21, 9},
            {27, 1},};
        cells = 25;
        size = new int[][]{
            {600, 500},};
        XY = new int[][]{
            {50, 450},};
        path = "src/Image/board6.jpg";
        maps[0] = new Map(ladder, snakes, cells, size, path, XY);
        //set starting coordinate of the player
        for (int i = 0; i < noPlayers; i++) {
            players[i].setXY(maps[0].getXY()[0][0], maps[0].getXY()[0][1]);
        }
        mapNo = 0;  //set current map
    }

    public void setNoPlayers(int num) {

        //when number of player change recreate player array
        players = new Player[num];
        noPlayers = num;
        for (int i = 0; i < num; i++) {
            players[i] = new Player("Player " + (i + 1));
        }
    }

    public void setView(TheView view) {
        this.view = view;
    }

    public Player getPlayer(int i) {
        return players[i];
    }

    public int getNoPlayers() {
        return noPlayers;
    }

    public int getTurn() {
        return turn;
    }

    public void setTurn() {
        if (turn == noPlayers - 1) {
            turn = 0;
        } else {
            turn++;
        }
    }

    public Map getMap(int i) {
        return maps[i];
    }

    public int getMapNo() {
        return mapNo;
    }

    public void setPosition(int die) {

        //set new position for player
        players[turn].setCurrPosition(die, maps[mapNo].getCells());
        boolean check = false;

        //check for ladder
        for (int i = 0; i < maps[mapNo].getLadder().length; i++) {
            if (maps[mapNo].getLadder()[i][0] == players[turn].getCurrPosition()) {
                check = true;
                Message ladder = new Message(this); //print message inform player
                ladder.printLadder();
                players[turn].setPosition(maps[mapNo].getLadder()[i][1]); //change position
                setChanged();
                notifyObservers();
                setTurn(); //change Player turn
            }
        }

        //check for snake
        for (int i = 0; i < maps[mapNo].getSnakes().length; i++) {
            if (maps[mapNo].getSnakes()[i][0] == players[turn].getCurrPosition()) {
                check = true;
                Message snake = new Message(this); //print message inform player
                snake.printSnake();
                players[turn].setPosition(maps[mapNo].getSnakes()[i][1]); //change position
                setChanged();
                notifyObservers();
                setTurn(); //change player turn
            }
        }

        //did not meet ladder and snake
        if (check == false) {
            if (players[turn].getCurrPosition() == maps[mapNo].getCells()) { //reach goal
                players[turn].setWinCounter();                               //set win counter
                this.view.roll.setEnabled(false);
                setChanged();
                notifyObservers();
                Message winner = new Message(this);     //print message inform player
                winner.printWinner(players[turn].getName());
            } else {
                setChanged();
                notifyObservers();
                setTurn(); //change player turn
            }
        }
    }

    public void resetGame() {

        //when finish 1 board
        //go to new board

        if (mapNo == maps.length - 1) {
            mapNo = 0;
        } else {
            mapNo++;
        }
        for (int i = 0; i < noPlayers; i++) {
            players[i].setXY(maps[mapNo].getXY()[0][0], maps[mapNo].getXY()[0][0]); //reset starting position
        }
        setChanged();
        notifyObservers();
        this.view.roll.setEnabled(true);
    }

    public void reset() {

        //when player click reset

        for (int i = 0; i < noPlayers; i++) {
            players[i] = new Player("Player " + (i + 1)); //recreate player array
            players[i].resetWinCounter(); //reset win counter
            players[i].setPosition(1); //reset position
            players[i].setXY(maps[0].getXY()[0][0], maps[0].getXY()[0][0]); //reset coordinate;
        }
        turn = 0;
        mapNo = 0;
        setChanged();
        notifyObservers();
    }
}
