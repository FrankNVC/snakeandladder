
public class Map {

    private int[][] ladder;
    private int[][] snakes;
    private int cells;
    private int[][] size;
    private String path;
    private int[][] XY;

    public Map() {
    }

    public Map(int[][] ladder, int[][] snakes, int cells, int[][] size, String path, int[][] XY) {
        this.ladder = ladder;
        this.snakes = snakes;
        this.cells = cells;
        this.size = size;
        this.path = path;
        this.XY = XY;
    }

    public int[][] getLadder() {
        return ladder;
    }

    public int[][] getSnakes() {
        return snakes;
    }

    public int getCells() {
        return cells;
    }

    public int[][] getSize() {
        return size;
    }

    public String getPath() {
        return path;
    }

    public int[][] getXY() {
        return XY;
    }
}
