
import javax.swing.Icon;

public class Player {

    private String name;
    private int currPosition;
    private int winCounter;
    private int x = 39;
    private int y = 739;

    public Player() {
    }

    public Player(String name) {
        this.name = name;
        this.currPosition = 1;
        this.winCounter = 0;
    }

    public String getName() {
        return name;
    }

    public int getCurrPosition() {
        return currPosition;
    }

    public void setCurrPosition(int die, int cell) {
        if ((currPosition + die) >= cell) {
            currPosition = cell;
        } else {
            currPosition += die;
        }
    }

    public void setPosition(int newPosition) {
        currPosition = newPosition;
    }

    public int getWinCounter() {
        return winCounter;
    }

    public void setWinCounter() {
        winCounter++;
    }

    public void resetWinCounter() {
        winCounter = 0;
    }

    public void resetGame() {
        currPosition = 1;
    }

    public void setXY(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
