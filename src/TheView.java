
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;
import javax.swing.*;
import javax.swing.border.EmptyBorder;

public class TheView extends JFrame implements Observer {

    NewPanel pn1 = new NewPanel(); //panel to draw game board
    JPanel pn2 = new JPanel(new BorderLayout()); // east panel to keep win counter/ roll dice/ reset game/ choose number of players
    JPanel pn21 = new JPanel(); //win counter component
    JPanel pn22 = new JPanel(); //die and button component
    JPanel pn23 = new JPanel(new GridLayout(2, 2));// no of players component
    JPanel pn3 = new JPanel(); //current position of player
    JButton roll = new JButton("Roll"); //roll button
    JButton reset = new JButton("Reset"); //reset button
    DiePanel dpn = new DiePanel(); //dice image
    JRadioButton b1 = new JRadioButton("2 Players", true);
    JRadioButton b2 = new JRadioButton("3 Players");
    JRadioButton b3 = new JRadioButton("4 Players");
    JRadioButton b4 = new JRadioButton("6 Players");
    BoardModel model;
    int x = 39;
    int y = 739;
    int die = 1;

    public void setModel(BoardModel model) {
        this.model = model;
    }

    public TheView(BoardModel newModel) {

        this.model = newModel; //connect to model

        this.setBackground(Color.LIGHT_GRAY);
        this.setTitle("Snakes And Ladders");

        pn1.setPreferredSize(new Dimension(600, 500));
        pn1.setLayout(new GridLayout(1, 1));

        pn21.setLayout(new GridLayout(this.model.getNoPlayers(), 2)); //create win counter and token of players
        for (int i = 0; i < this.model.getNoPlayers(); i++) {
            JLabel win = new JLabel(model.getPlayer(i).getName() + ": " + model.getPlayer(i).getWinCounter() + " wins");
            IconPanel token = new IconPanel(model.path[i]);
            token.setPreferredSize(new Dimension(40, 40));
            pn21.add(win);
            pn21.add(token);
        }

        roll.setPreferredSize(new Dimension(100, 30)); //create roll button
        roll.setBorder(new EmptyBorder(10, 10, 10, 10));
        pn22.add(roll);

        reset.setPreferredSize(new Dimension(100, 30)); //create roll button
        reset.setBorder(new EmptyBorder(10, 10, 10, 10));
        pn22.add(reset);

        JPanel pn = new JPanel(new GridLayout(2, 1)); //draw dice image

        pn.add(dpn);
        pn.add(pn22);

        //add radio checkbox for no of players
        pn23.add(b1);
        pn23.add(b2);
        pn23.add(b3);
        pn23.add(b4);

        //add listener to radio checkbox
        b1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                model.setNoPlayers(2);
                model.reset();
            }
        });

        b2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                model.setNoPlayers(3);
                model.reset();
            }
        });

        b3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                model.setNoPlayers(4);
                model.reset();
            }
        });

        b4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                model.setNoPlayers(6);
                model.reset();
            }
        });

        ButtonGroup group = new ButtonGroup();
        group.add(b1);
        group.add(b2);
        group.add(b3);
        group.add(b4);

        pn2.add(pn21, BorderLayout.NORTH);
        pn2.add(pn, BorderLayout.CENTER);
        pn2.add(pn23, BorderLayout.SOUTH);
        pn2.setPreferredSize(new Dimension(225, 500));

        this.getContentPane().add(pn1, BorderLayout.CENTER);
        this.getContentPane().add(pn2, BorderLayout.EAST);

        pn3.setLayout(new GridLayout(this.model.getNoPlayers(), 2));
        for (int i = 0; i < this.model.getNoPlayers(); i++) {
            JLabel status = new JLabel(model.getPlayer(i).getName() + " moves to: " + model.getPlayer(i).getCurrPosition());
            pn3.add(status);
        }

        this.getContentPane().add(pn3, BorderLayout.SOUTH);

        validate();
        repaint();
    }

    @Override
    @SuppressWarnings("unchecked")
    public void update(Observable o, Object arg) {

        //when notified by model process x and y coordinate

        //set x and y coordinate for each player in map size 600
        if (this.model.getMap(model.getMapNo()).getSize()[0][0] == 600) {
            for (int j = 0; j < this.model.getNoPlayers(); j++) {
                x = model.getMap(model.getMapNo()).getXY()[0][0];
                y = model.getMap(model.getMapNo()).getXY()[0][1];
                int position = this.model.getPlayer(j).getCurrPosition();

                for (int i = 2; i <= position; i++) {
                    if ((2 <= i && i <= 6) || (14 <= i && i <= 18) || (26 <= i && i <= 30)) {
                        x += 100;
                    } else if ((8 <= i && i <= 12) || (20 <= i && i <= 24)) {
                        x -= 100;
                    }
                }
                if (position <= 6) {
                    y = 450;
                } else if (position <= 12) {
                    y = 350;
                } else if (position <= 18) {
                    y = 250;
                } else if (position <= 24) {
                    y = 150;
                } else {
                    y = 50;
                }
                x -= (5 * j);
                model.getPlayer(j).setXY(x, y);
            }
        } else if (this.model.getMap(model.getMapNo()).getSize()[0][0] == 640) { //set x and y coordinate for each player in map size 640
            for (int j = 0; j < this.model.getNoPlayers(); j++) {
                x = model.getMap(model.getMapNo()).getXY()[0][0];
                y = model.getMap(model.getMapNo()).getXY()[0][1];
                int position = this.model.getPlayer(j).getCurrPosition();

                for (int i = 2; i <= position; i++) {
                    if ((2 <= i && i <= 8) || (18 <= i && i <= 24) || (34 <= i && i <= 40) || (50 <= i && i <= 56)) {
                        x += 80;
                    } else if ((10 <= i && i <= 16) || (26 <= i && i <= 32) || (42 <= i && i <= 48) || (58 <= i && i <= 64)) {
                        x -= 80;
                    }
                }
                if (position <= 8) {
                    y = 599;
                } else if (position <= 16) {
                    y = 519;
                } else if (position <= 24) {
                    y = 439;
                } else if (position <= 32) {
                    y = 359;
                } else if (position <= 40) {
                    y = 279;
                } else if (position <= 48) {
                    y = 199;
                } else if (position <= 56) {
                    y = 119;
                } else {
                    y = 39;
                }
                x -= (5 * j);
                model.getPlayer(j).setXY(x, y);
            }
        } else {
            for (int j = 0; j < this.model.getNoPlayers(); j++) { //set x and y coordinate for each player in map size 800
                x = model.getMap(model.getMapNo()).getXY()[0][0];
                y = model.getMap(model.getMapNo()).getXY()[0][1];
                int position = this.model.getPlayer(j).getCurrPosition();

                for (int i = 2; i <= position; i++) {
                    if ((2 <= i && i <= 10) || (22 <= i && i <= 30) || (42 <= i && i <= 50) || (62 <= i && i <= 70) || (82 <= i && i <= 90)) {
                        x += 80;
                    } else if ((12 <= i && i <= 20) || (32 <= i && i <= 40) || (52 <= i && i <= 60) || (72 <= i && i <= 80) || (92 <= i && i <= 100)) {
                        x -= 80;
                    }
                }
                if (position <= 10) {
                    y = 739;
                } else if (position <= 20) {
                    y = 659;
                } else if (position <= 30) {
                    y = 579;
                } else if (position <= 40) {
                    y = 499;
                } else if (position <= 50) {
                    y = 419;
                } else if (position <= 60) {
                    y = 339;
                } else if (position <= 70) {
                    y = 259;
                } else if (position <= 80) {
                    y = 179;
                } else if (position <= 90) {
                    y = 99;
                } else {
                    y = 19;
                }
                x -= (5 * j);
                model.getPlayer(j).setXY(x, y);
            }
        }
        drawBoard();
    }

    public void drawBoard() { //redraw the entire frame

        pn1.setPreferredSize(new Dimension(model.getMap(model.getMapNo()).getSize()[0][0], model.getMap(model.getMapNo()).getSize()[0][1]));
        pn1.repaint();
        this.getContentPane().add(pn1, BorderLayout.CENTER);

        this.getContentPane().remove(pn2);
        pn2.removeAll();
        pn2.revalidate();
        pn21.removeAll();
        pn21.revalidate();
        pn22.removeAll();
        pn22.revalidate();

        pn21.setLayout(new GridLayout(this.model.getNoPlayers(), 2));
        for (int i = 0; i < this.model.getNoPlayers(); i++) {
            JLabel win = new JLabel(model.getPlayer(i).getName() + ": " + model.getPlayer(i).getWinCounter() + " wins");
            IconPanel token = new IconPanel(model.path[i]);
            token.setPreferredSize(new Dimension(40, 40));
            pn21.add(win);
            pn21.add(token);
        }

        roll.setPreferredSize(new Dimension(100, 30));
        roll.setBorder(new EmptyBorder(10, 10, 10, 10));
        pn22.add(roll);

        reset.setPreferredSize(new Dimension(100, 30));
        reset.setBorder(new EmptyBorder(10, 10, 10, 10));
        pn22.add(reset);

        JPanel pn = new JPanel(new GridLayout(2, 1));

        pn.add(dpn);
        pn.add(pn22);

        pn23.removeAll();
        pn23.revalidate();

        pn23.add(b1);
        pn23.add(b2);
        pn23.add(b3);
        pn23.add(b4);

        b1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                model.setNoPlayers(2);
                model.reset();
            }
        });

        b2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                model.setNoPlayers(3);
                model.reset();
            }
        });

        b3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                model.setNoPlayers(4);
                model.reset();
            }
        });

        b4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                model.setNoPlayers(6);
                model.reset();
            }
        });

        ButtonGroup group = new ButtonGroup();
        group.add(b1);
        group.add(b2);
        group.add(b3);
        group.add(b4);

        pn2.add(pn21, BorderLayout.NORTH);
        pn2.add(pn, BorderLayout.CENTER);
        pn2.add(pn23, BorderLayout.SOUTH);
        pn2.setPreferredSize(new Dimension(225, model.getMap(model.getMapNo()).getSize()[0][1]));
        this.getContentPane().add(pn2, BorderLayout.EAST);

        this.getContentPane().remove(pn3);
        pn3.removeAll();
        pn3.revalidate();
        pn3.setLayout(new GridLayout(this.model.getNoPlayers(), 2));
        for (int i = 0; i < this.model.getNoPlayers(); i++) {
            JLabel status = new JLabel(model.getPlayer(i).getName() + " moves to: " + model.getPlayer(i).getCurrPosition());
            pn3.add(status);
        }
        this.getContentPane().add(pn3, BorderLayout.SOUTH);

        this.validate();
        this.repaint();
        this.pack();
        this.setLocationRelativeTo(null);
    }

    class NewPanel extends JPanel {

        @Override
        protected void paintComponent(Graphics g) {

            //draw board game
            super.paintComponent(g);

            setPreferredSize(new Dimension(model.getMap(model.getMapNo()).getSize()[0][0], model.getMap(model.getMapNo()).getSize()[0][1]));

            ImageIcon imageIcon = new ImageIcon(model.getMap(model.getMapNo()).getPath());
            Image boardImage = imageIcon.getImage();

            g.drawImage(boardImage, 0, 0, model.getMap(model.getMapNo()).getSize()[0][0], model.getMap(model.getMapNo()).getSize()[0][1], null);

            for (int i = 0; i < model.getNoPlayers(); i++) {
                imageIcon = new ImageIcon(model.path[i]);
                Image tokenImage = imageIcon.getImage();

                g.drawImage(tokenImage, model.getPlayer(i).getX(), model.getPlayer(i).getY(), 40, 40, null);
            }
        }
    }

    class DiePanel extends JPanel {

        private String path;

        @Override
        protected void paintComponent(Graphics g) {

            //draw die number image
            super.paintComponent(g);

            switch (die) {
                case 1:
                    path = "src/Image/1.png";
                    break;
                case 2:
                    path = "src/Image/2.png";
                    break;
                case 3:
                    path = "src/Image/3.png";
                    break;
                case 4:
                    path = "src/Image/4.png";
                    break;
                case 5:
                    path = "src/Image/5.png";
                    break;
                case 6:
                    path = "src/Image/6.png";
                    break;
            }

            ImageIcon imageIcon = new ImageIcon(path);
            Image dieImage = imageIcon.getImage();

            g.drawImage(dieImage, 50, 50, 100, 100, null);

        }
    }

    public void setDie(int die) {
        this.die = die;
        this.dpn.repaint();
    }

    class IconPanel extends JPanel {

        private String path;

        public IconPanel(String path) {
            this.path = path;
        }

        @Override
        protected void paintComponent(Graphics g) {

            //draw player token
            super.paintComponent(g);

            ImageIcon imageIcon = new ImageIcon(path);
            Image tokenImage = imageIcon.getImage();

            g.drawImage(tokenImage, 0, 0, 20, 20, null);
        }
    }
}
