
import java.awt.Button;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Message extends MouseAdapter {

    private BoardModel model = new BoardModel();
    private JFrame frame = new JFrame();
    private JPanel panel = new JPanel();
    private JLabel label = new JLabel();

    public Message(BoardModel model) {
        this.model = model;
    }

    public void printWinner(String text) {

        frame = new JFrame();
        frame.setSize(300, 100);
        frame.setTitle("Winner!");

        label.setText(text + " is the Winner");
        panel.add(label);

        frame.setLayout(new FlowLayout());
        frame.add(panel);

        Button btnOK = new Button("OK");
        btnOK.addMouseListener(this); //add listener
        frame.add(btnOK);

        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        frame.setAlwaysOnTop(true);
    }

    public void printLadder() {

        //print message inform player about ladder
        frame = new JFrame();
        frame.setSize(300, 100);
        frame.setTitle("Ladder");

        label.setText("Yayy, You meet the ladder, Go Up");
        panel.add(label);

        frame.setLayout(new FlowLayout());
        frame.add(panel);

        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        frame.setAlwaysOnTop(true);
    }

    public void printSnake() {

        //print message inform player about snake
        frame = new JFrame();
        frame.setSize(300, 100);
        frame.setTitle("Snake");

        label.setText("Ooops, You meet the Snake, Go Down");
        panel.add(label);

        frame.setLayout(new FlowLayout());
        frame.add(panel);

        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        frame.setAlwaysOnTop(true);
    }

    @Override
    public void mouseClicked(MouseEvent e) {

        //when finish 1 game
        for (int i = 0; i < this.model.getNoPlayers(); i++) {
            this.model.getPlayer(i).resetGame(); //reset players starting position and coordinate
        }
        this.model.resetGame(); //reset game, change board
        frame.setVisible(false);

    }
}
