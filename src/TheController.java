
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Random;
import javax.swing.*;

public class TheController extends MouseAdapter {

    private BoardModel model;
    private TheView view;
    private Random random = new Random();
    int die = 0;

    public TheController(BoardModel model) {
        this.model = model;
    }

    public BoardModel getModel() {
        return model;
    }

    public void setView(TheView view) {
        this.view = view;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        //reset die
        die = 0;

        if (e.getSource() == view.roll) { //roll is clicked
            while (die <= 0) {
                die = random.nextInt(7);
            }
            model.setPosition(die); //change position in model
            view.setDie(die); //change the die number in view
        } else if (e.getSource() == view.reset) { //reset is clicked
            model.reset(); //reset all data in model
        }
    }
}
