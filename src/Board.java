
public class Board {

    private int noOfCell;
    private int[][] ladder = new int[][]{};
    private int[][] snake = new int[][]{};

    public Board(int noOfCell, int[][] ladder, int[][] snake) {
        this.noOfCell = noOfCell;
        this.ladder = ladder;
        this.snake = snake;
    }

    public int getNoOfCell() {
        return noOfCell;
    }

    public int[][] getLadder() {
        return ladder;
    }

    public int[][] getSnake() {
        return snake;
    }
}
