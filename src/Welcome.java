
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Welcome extends JFrame implements ActionListener {

    BoardModel model = new BoardModel(2);
    TheView view = new TheView(model);
    TheController control = new TheController(model);
    PicturePanel ppn = new PicturePanel();
    JButton play = new JButton("Play");
    JButton exit = new JButton("Exit");

    public Welcome() {
        this.setPreferredSize(new Dimension(400, 650));
        this.setLocationRelativeTo(null);

        MessagePanel heading = new MessagePanel();
        heading.setPreferredSize(new Dimension(300, 50));

        this.add(heading, BorderLayout.NORTH);

        heading.setFont(new Font("Arial", Font.BOLD, 20));

        this.add(ppn, BorderLayout.CENTER);

        JPanel pn = new JPanel();
        pn.setLayout(new FlowLayout());

        play.addActionListener(this); //add play button listener
        exit.addActionListener(this); //add exit button listener

        pn.add(play);
        pn.add(exit);

        this.add(pn, BorderLayout.SOUTH);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == this.play) { //process and connect theView, theController and theModel
            model.addObserver(view);
            model.setView(view);
            control.setView(view);

            view.roll.addMouseListener(control);
            view.reset.addMouseListener(control);

            view.pack();
            view.setLocationRelativeTo(null);
            view.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            view.setVisible(true);

            this.setVisible(false);
        } else {
            this.setVisible(false);
        }
    }

    class MessagePanel extends JPanel {

        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);

            FontMetrics fm = g.getFontMetrics();

            int stringWidth = fm.stringWidth("Welcome to Snakes and Ladders"); //get String width
            int stringAscent = fm.getAscent(); //get String height

            int xCoordinate = getWidth() / 2 - stringWidth / 2; //find x coordinate of center location
            int yCoordinate = getHeight() / 2 - stringAscent / 2; // find y coordinate of center location

            g.drawString("Welcome to Snakes and Ladders", xCoordinate, yCoordinate);
        }
    }

    class PicturePanel extends JPanel {

        @Override
        protected void paintComponent(Graphics g) {

            super.paintComponent(g);

            ImageIcon imageIcon = new ImageIcon("src/Image/snake2.jpg");// create the welcome image
            Image welcomeImage = imageIcon.getImage();

            g.drawImage(welcomeImage, 0, 0, 400, 500, null); //draw the welcome image
        }
    }
}
