
import java.util.Random;

public class TextSnakesAndLadders {

    public static void main(String[] args) {

        //create board
        Board board = new Board(9, new int[][]{{3, 7}}, new int[][]{{9, 5}});
        //set starting position
        int iPosition1 = 0;
        int iPosition2 = 0;

        //create die
        Random r = new Random();
        int dice = 0;

        System.out.println("This board has 9 cells");
        System.out.println("Snakes and ladders:");
        System.out.println("3 ==> 7");
        System.out.println("board.getNoOfCell() ==> 5");

        for (int i = 0; i <= board.getNoOfCell(); i++) {
            System.out.print(i + "\t");
        }
        System.out.println("Goal");
        System.out.println("0");
        System.out.println("1");
        System.out.println("");

        while (iPosition1 <= board.getNoOfCell() && iPosition2 <= board.getNoOfCell()) {

            //roll dice
            while (dice <= 0) {
                dice = r.nextInt(6);
            }
            //change position
            iPosition1 += dice;
            System.out.println("Player 0 : move : " + dice);
            //check goal
            if (iPosition1 > board.getNoOfCell()) {
                iPosition1 = 10;
                break;
            } else if (iPosition1 == board.getLadder()[0][0]) { //check ladder
                iPosition1 = board.getLadder()[0][0];
            } else if (iPosition1 == board.getSnake()[0][0]) { //check snake
                iPosition1 = board.getSnake()[0][1];
            }

            dice = 0;
            //roll dice
            while (dice <= 0) {
                dice = r.nextInt(6);
            }
            //change position
            iPosition2 += dice;
            System.out.println("Player 1 : move : " + dice);
            //check goal
            if (iPosition2 > board.getNoOfCell()) {
                iPosition2 = 10;
                break;
            } else if (iPosition2 == board.getLadder()[0][0]) { //check ladder
                iPosition2 = board.getLadder()[0][1];
            } else if (iPosition2 == board.getSnake()[0][0]) { //check snake
                iPosition2 = board.getSnake()[0][1];
            }
            
            //print board
            for (int i = 0; i <= board.getNoOfCell(); i++) {
                System.out.print(i + "\t");
            }
            System.out.println("Goal");
            for (int j = 1; j <= iPosition1; j++) {
                System.out.print("\t");
            }
            System.out.println("0");
            for (int j = 1; j <= iPosition2; j++) {
                System.out.print("\t");
            }
            System.out.println("1");
            System.out.println("");
        }

        for (int i = 0; i <= board.getNoOfCell(); i++) {
            System.out.print(i + "\t");
        }
        System.out.println("Goal");
        for (int j = 1; j <= iPosition1; j++) {
            System.out.print("\t");
        }
        System.out.println("0");
        for (int j = 1; j <= iPosition2; j++) {
            System.out.print("\t");
        }
        System.out.println("1");
        System.out.println("");

        if (iPosition1 > board.getNoOfCell()) {
            System.out.println("The winner is player 0");
        } else {
            System.out.println("The winner is player 1");
        }
    }
}
