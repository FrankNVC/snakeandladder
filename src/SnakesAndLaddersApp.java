
import java.util.HashSet;
import java.util.Set;
import javax.swing.JFrame;

public class SnakesAndLaddersApp {

    public SnakesAndLaddersApp() {
        Welcome welcome = new Welcome();
        welcome.pack();
        welcome.setLocationRelativeTo(null);
        welcome.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        welcome.setVisible(true);
    }

    public static void main(String[] args) {
        SnakesAndLaddersApp app = new SnakesAndLaddersApp();
    }
}
